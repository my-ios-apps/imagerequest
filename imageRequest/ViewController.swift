//
//  ViewController.swift
//  imageRequest
//
//  Created by Mario Cezzare on 05/01/19.
//  Copyright © 2019 Mario Cezzare. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelResult: UILabel!
    
    let imageUrls = [
        "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-2b.jpg",
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func handleLoadImageButtonPress(_ sender: UIButton ){
        //MARK : Button OK have tag = 0 and Button not ok have tag = 1 
        var optionArray: Int!
        optionArray = sender.tag

        var colorResults = [UIColor]()
        colorResults.append(.green)
        colorResults.append(.red)
        colorResults.append(.black)
        var colorResult = 0
        
        guard let imageUrl = URL(string: imageUrls[optionArray]) else {
            print("can't load url!!")
            return
        }
        self.labelResult.textColor = colorResults[2]
        self.labelResult.text = "Loading..."
        var statusDownload = "Done"

        let task = URLSession.shared.dataTask(with: imageUrl){ (data,response,error) in
            guard let data = data else {
                print("no data , or there no error")
                return
            }
            
            let downloadedImage = UIImage(data: data)
            if let error = error {
                self.labelResult.text = error.localizedDescription
            } else if let response = response as? HTTPURLResponse,
                response.statusCode == 404 {
                statusDownload = "Fail"
                colorResult = 1
            }
            
            DispatchQueue.main.async {
                self.imageView.image = downloadedImage
                self.labelResult.text = statusDownload
                self.labelResult.textColor = colorResults[colorResult]
            }
            
            print("data: \(data)")
            print("response: \(String(describing: response))")
            print("error: \(String(describing: error))")
            
        }
        task.resume()
    }
    
    
    
    
    
}

